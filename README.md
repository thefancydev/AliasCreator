<h2>What is it?</h2>
AliasCreator is a windows command line utility that allows you to create and delete command aliases easily
<h2>What can it do</h2>
<ul>
<li>Create aliases with argument support</li>
<li>Delete aliases</li>
<li>List aliases</li>
</ul>
<h2>Contributing</h2>
To contribute to this project, all you need to do is to fork the repo 
to your own gitlab account, make your changes, then generate a pull 
request on your repo. I will review the request and merge it if it passes the test.
<h2>Issues</h2>
Report issues on the Report page