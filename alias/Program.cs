﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Xml;
using System.Text;
using System.Data;
using MarkdownLog;
using System.Threading;
using System.Net.Mail;
using System.Xml.Serialization;

namespace alias
{
    public class Gmail
    {
        public string Username { get; set; }
        public string Password { get; set; }

        public Gmail(string username, string password)
        {
            Username = username;
            Password = password;
        }

        public void Send(MailMessage msg)
        {
            SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
            client.EnableSsl = true;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.Credentials = new NetworkCredential(Username, Password);
            client.Send(msg);
        }
    }

    public class ProgressBar : IDisposable, IProgress<double>
    {
        private const int blockCount = 20;
        private readonly TimeSpan animationInterval = TimeSpan.FromSeconds(1.0 / 5);
        private const string animation = @"|/-\";

        private readonly Timer timer;

        private double currentProgress = 0;
        private string currentText = string.Empty;
        private bool disposed = false;
        private int animationIndex = 0;

        public ProgressBar()
        {
            timer = new Timer(TimerHandler);

            if (!Console.IsOutputRedirected)
            {
                ResetTimer();
            }
        }

        public void Report(double value)
        {
            // Make sure value is in [0..1] range
            value = Math.Max(0, Math.Min(1, value));
            Interlocked.Exchange(ref currentProgress, value);
        }

        private void TimerHandler(object state)
        {
            lock (timer)
            {
                if (disposed) return;

                int progressBlockCount = (int)(currentProgress * blockCount);
                int percent = (int)(currentProgress * 100);
                string text = string.Format("[{0}{1}] {2,3}% {3}",
                    new string('#', progressBlockCount), new string('-', blockCount - progressBlockCount),
                    percent,
                    animation[animationIndex++ % animation.Length]);
                UpdateText(text);

                ResetTimer();
            }
        }

        private void UpdateText(string text)
        {
            // Get length of common portion
            int commonPrefixLength = 0;
            int commonLength = Math.Min(currentText.Length, text.Length);
            while (commonPrefixLength < commonLength && text[commonPrefixLength] == currentText[commonPrefixLength])
            {
                commonPrefixLength++;
            }

            // Backtrack to the first differing character
            StringBuilder outputBuilder = new StringBuilder();
            outputBuilder.Append('\b', currentText.Length - commonPrefixLength);

            // Output new suffix
            outputBuilder.Append(text.Substring(commonPrefixLength));

            // If the new text is shorter than the old one: delete overlapping characters
            int overlapCount = currentText.Length - text.Length;
            if (overlapCount > 0)
            {
                outputBuilder.Append(' ', overlapCount);
                outputBuilder.Append('\b', overlapCount);
            }

            Console.Write(outputBuilder);
            currentText = text;
        }

        private void ResetTimer()
        {
            timer.Change(animationInterval, TimeSpan.FromMilliseconds(-1));
        }

        public void Dispose()
        {
            lock (timer)
            {
                disposed = true;
                UpdateText(string.Empty);
            }
        }

    }

    public class Command
    {
        public string name { get; set; }
        public string description { get; set; }
        public string[] arguments { get; set; }
        public string executed { get; set; }
    }

    public class formattedCommand
    {
        public string name { get; set; }
        public string description { get; set; }
        public string arguments { get; set; }
    }
    internal class Program
    {
        readonly static string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), "AppData", "Roaming", "aliases");
        readonly static string installedRosterPath = Path.Combine(path, "commandRoster.xml");
        readonly static string installedConfigPath = Path.Combine(path, "alias.config");
        readonly static int version = 10;

        readonly static Dictionary<string, KeyValuePair<string, int>> systemCommands = new Dictionary<string, KeyValuePair<string, int>>()
        {
            //? the keyvalue pair contains description then the number of arguments accepted
            {"remove", new KeyValuePair<string, int>("removes an alias", 1)},
            {"make", new KeyValuePair<string, int>("creates an alias", 4)},
            {"view", new KeyValuePair<string, int>("shows all aliases", 0)},
            {"install", new KeyValuePair<string, int>("install a shared alias (list with 'alias common')", 1)},
            {"common", new KeyValuePair<string, int>("show common alias commands", 0)},
            {"export", new KeyValuePair<string, int>("Export the given alias to a .alias file", 1)},
            {"submit", new KeyValuePair<string, int>("submit a command to the developer (me)", 1)},
            {"uninstall", new KeyValuePair<string, int>("Removes the Alias utility", 0)},
            {"review", new KeyValuePair<string, int>("edit existing command", 1)},
            {"load", new KeyValuePair<string, int>("Load a .alias file", 1)},
            {"config", new KeyValuePair<string, int>("View configuration or change setting", 0)},
            {"full", new KeyValuePair<string, int>("create a command and edit in notepad", 1)},
            {"wipe", new KeyValuePair<string, int>("Removes all aliases", 0)},
            {"about", new KeyValuePair<string, int>("Takes you to the Alias homepage", 0)},
            {"update", new KeyValuePair<string, int>("Updates alias", 0)}
        };

        static string subcommand; //one of the above systemCommands

        static string[] arguments; //the arguments provided for the subcommand

        private static void progress()
        {
            using (var progressBar = new ProgressBar())
            {
                for (int i = 0; i < 100; i++)
                {
                    progressBar.Report((double)i / 100);
                    Thread.Sleep(20);
                }
            }

            Console.WriteLine("Done!");
        }

        private static bool? getConfig(string configName)
        {
            XmlDocument doc = new XmlDocument();

            doc.Load(installedConfigPath);
            try
            {
                var setting = doc.SelectSingleNode($"/config/setting[name='{configName}']");

                if (setting == null)
                {
                    return null;
                }
                else
                {
                    return bool.Parse(setting.SelectSingleNode("value").InnerText);
                }
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Entry point for application
        /// </summary>
        /// <param name="args"></param>
        private static void Main(string[] args)
        {
            string[] debugCommand = null; //set this to test a command (set to command name then to parameters)

            if (debugCommand != null)
                args = debugCommand;

            if (!Directory.Exists(path)) //if the application folder doesn't exist
                InstallApp();
            else
                if (args.Length == 0)
                DisplayHelp(true);
            else
            {
                subcommand = args[0];
                arguments = args.Where(x => !x.Equals(subcommand)).ToArray(); //? gets all arguments minus the subcommand

                try
                {
                    ParseSubCommands();
                }
                catch (Exception e)
                {
                    if (getConfig("UsageLogs") == true)
                    {
                        Gmail sender = new Gmail("no-reply@octopusnetwork.co.uk", "$yntaxC0de"); //do not bother trying to log in, as I will change the passeord, and if you try to again, I will chabge it again, and again, and again. You get the picture :-)

                        MailMessage msg = new MailMessage("no-reply@octopusnetwork.co.uk", "rhys0178@gmail.com");
                        msg.Subject = "Alias error";

                        XmlDocument doc = new XmlDocument();

                        XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
                        XmlElement root = doc.DocumentElement;
                        doc.InsertBefore(xmlDeclaration, root);

                        XmlElement exceptionRoot = doc.CreateElement(string.Empty, "Exception", string.Empty);
                        doc.AppendChild(exceptionRoot);

                        foreach(var property in e.GetType().GetProperties().Where(x => !x.GetType().IsArray && x.GetValue(e) != null))
                        {
                            XmlElement propertyElement = doc.CreateElement(string.Empty, property.Name, string.Empty);
                            XmlText propertyValue = doc.CreateTextNode((string)property.GetValue(e).ToString());
                            propertyElement.AppendChild(propertyValue);
                            exceptionRoot.AppendChild(propertyElement);
                        }

                        msg.Body = doc.OuterXml;

                        sender.Send(msg);
                    }

                    throw;
                }
            }
        }

        /// <summary>
        /// Gets all the commands available from the internet
        /// </summary>
        /// <returns></returns>
        private static IEnumerable<Command> GetAllCommonCommands()
        {
            const string onlineRosterURL = "https://gitlab.com/roconnor/AliasCreator/raw/master/exports/commandRoster.xml";

            string xmlStr;

            using (var wc = new WebClient())
            {
                xmlStr = wc.DownloadString(onlineRosterURL);
            }
            var xmlDoc = new XmlDocument();

            xmlDoc.LoadXml(xmlStr);

            XmlNodeList commands = xmlDoc.SelectNodes("/commandRoster/name");

            foreach (var name in commands)
            {
                var command = (XmlNode)name;
                string commandURL = $"https://gitlab.com/roconnor/AliasCreator/raw/master/exports/{command.InnerText}.alias";

                string xmlCommandStr;

                using (var wc = new WebClient())
                {
                    xmlCommandStr = wc.DownloadString(commandURL);
                }

                var xmlCommandDoc = new XmlDocument();

                xmlCommandDoc.LoadXml(xmlCommandStr);

                yield return new Command()
                {
                    name = xmlCommandDoc.SelectSingleNode("/command/name").InnerText,
                    description = xmlCommandDoc.SelectSingleNode("/command/description").InnerText,
                    arguments = xmlCommandDoc.SelectSingleNode("/command/arguments").InnerText.Split(';'),
                    executed = xmlCommandDoc.SelectSingleNode("/command/executed").InnerText
                };
            }
        }

        private static void DisplayHelp(bool quit)
        {
            foreach (var keyvaluepair in systemCommands)
                Console.WriteLine($"{keyvaluepair.Key} [{keyvaluepair.Value.Value} arguments]) {keyvaluepair.Value.Key}{Environment.NewLine}"); //? this will show up as "command [argument count]) command description"

            if (quit)
                Environment.Exit(0);
        }

        private static void ParseSubCommands()
        {
            switch (subcommand.ToLower())
            {
                case "config":
                    Config();
                    break;
                case "remove":
                    Remove();
                    break;

                case "make":
                    Make();
                    break;

                case "view":
                    View();
                    break;

                case "install":
                    InstallCommand();
                    break;

                case "wipe":
                    Wipe();
                    break;

                case "submit":
                    Submit();
                    break;

                case "update":
                    Update();
                    break;

                case "uninstall":
                    uninstall();
                    break;

                case "common":
                    ViewCommon();
                    break;

                case "review":
                    Review();
                    break;

                case "load":
                    LoadFile();
                    break;

                case "full":
                    Full();
                    break;

                case "export":
                    Export();
                    break;

                case "about":
                    Process.Start("https://gitlab.com/roconnor/AliasCreator");
                    break;

                case "marco": //? my little bit of fun :-)
                    Console.WriteLine("polo!");
                    break;

                default:
                    Console.WriteLine("This command doesn't exist!\r\n");
                    DisplayHelp(true);
                    break;
            }
        }

        private static void Update()
        {
            XmlDocument doc = new XmlDocument();

            doc.Load("https://gitlab.com/roconnor/AliasCreator/raw/master/releases/versionHistory.xml");
            var node = doc.SelectSingleNode("/versions/version[.='" + (version + 1).ToString() + "']");
            if (node == null)
                Console.WriteLine($"You already have the latest version of Alias installed (V {version})");
            else
            {
                Console.WriteLine($"New version of Alias found! Version {version + 1}. Do you wish to update? (Y/N): ");

                var assent = Console.ReadKey().Key;

                if (assent == ConsoleKey.Y)
                {
                    Console.WriteLine("Downloading alias");
                    using (var client = new WebClient())
                    {
                        client.DownloadFile($"https://gitlab.com/roconnor/AliasCreator/raw/master/releases/V{version + 1}/alias.exe", Path.Combine(path, "newalias.exe"));
                    }
                    progress();

                    Process.Start("cmd.exe",
                    $"/C choice /C Y /N /D Y /T 3 & Del /F /Q {Path.Combine(path, "alias.exe")} && REN {Path.Combine(path, "newalias.exe")} alias.exe");

                    Environment.Exit(0);
                }
            }
        }
        private static void Config()
        {
            if (arguments.Length == 0) //display all config settings
            {
                XmlDocument doc = new XmlDocument();

                doc.Load(installedConfigPath);

                var configsettings = doc.SelectNodes("/config/setting");
                List<dynamic> configs = new List<dynamic>();

                foreach (XmlNode setting in configsettings)
                {
                    Console.WriteLine($"{setting.SelectSingleNode("name").InnerText}: {setting.SelectSingleNode("value").InnerText}");
                }
            }
            else if (arguments.Length == 1)
            {
                XmlDocument doc = new XmlDocument();

                doc.Load(installedConfigPath);

                var configsetting = doc.SelectSingleNode($"/config/setting[name='{arguments[0]}']");

                if (configsetting != null)
                {
                    Console.WriteLine($"{configsetting.SelectSingleNode("name").InnerText}: {configsetting.SelectSingleNode("value").InnerText}");
                }
                else
                {
                    Console.WriteLine("This setting doesn't exist!");
                }
            }
            else if (arguments.Length == 2)
            {
                XmlDocument doc = new XmlDocument();

                doc.Load(installedConfigPath);

                XmlNode configsetting = doc.SelectSingleNode($"/config/setting[name='{arguments[0]}']");

                if (configsetting != null)
                {
                    configsetting.SelectSingleNode("value").InnerText = arguments[1];
                    doc.Save(installedConfigPath);
                }
                else
                {
                    Console.WriteLine("This setting doesn't exist!");
                }
            }
        }

        private static void Submit()
        {
            if (arguments.Length < 1)
            {
                Console.WriteLine("Please specify an alias name");
                Environment.Exit(0);
            }

            XmlDocument doc = new XmlDocument();

            doc.Load(installedRosterPath);

            var commandXML = doc.SelectSingleNode($"/commands/command[name='{arguments[0]}']").OuterXml;

            Gmail sender = new Gmail("roconnoremailservices@gmail.com", "3m@1l$ervices"); //do not bother trying to log in, as I will change the passeord, and if you try to again, I will chabge it again, and again, and again. You get the picture :-)

            MailMessage msg = new MailMessage("roconnoremailservices@gmail.com", "rhys.oconnor@outlook.com");
            msg.Subject = "Alias command submission";
            msg.Body = commandXML;
            sender.Send(msg);
        }

        private static void Wipe()
        {
            Console.WriteLine("Are you sure you want to reset configuration and delete all aliases? (Y/N): ");

            var assent = Console.ReadKey().Key;

            if (assent == ConsoleKey.Y)
            {
                string[] aliases = Directory.GetFiles(path, "*.bat");

                foreach (var alias in aliases)
                {
                    arguments = new string[] { Path.GetFileName(alias).Replace(".bat", "") };
                    Remove();
                }
            }
        }

        private static void Export()
        {
            if (getConfig("AllowExport") == false || getConfig("AllowExport") == null)
            {
                Console.WriteLine("This isn't allowed. If this is a mistake, run 'alias config AllowExport true'");
                Environment.Exit(0);
            }

            if (arguments.Length < 1)
            {
                Console.WriteLine("Please supply Alias name to export");
                Environment.Exit(0);
            }

            if (!File.Exists(Path.Combine(path, arguments[0] + ".bat")))
            {
                Console.WriteLine("This alias doesn't exist!");
            }

            else
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(installedRosterPath);

                XmlNode command = doc.SelectSingleNode($"/commands/command[name='{arguments[0]}']");

                if (command != null)
                {
                    Console.WriteLine($"Command found. Exporting to {arguments[0]}.alias...");
                    File.WriteAllText(Path.Combine(Environment.CurrentDirectory, arguments[0] + ".alias"), command.OuterXml);
                    progress();
                }
                else
                {
                    Console.WriteLine("This command doesn't exist!");
                }
            }
        }

        private static void LoadFile()
        {
            if (getConfig("AllowLoad") == false || getConfig("AllowLoad") == null)
            {
                Console.WriteLine("This isn't allowed. If this is a mistake, type 'alias config AllowLoad true'");
                Environment.Exit(0);
            }

            if (arguments.Length < 1)
            {
                Console.WriteLine("Please supply .alias file location");
                Environment.Exit(0);
            }

            if (!File.Exists(arguments[0]))
            {
                Console.WriteLine("This file doesn't exist!");
            }
            else if (Path.GetExtension(arguments[0]) != ".alias")
            {
                Console.WriteLine("This isn't the correct format. Alias only accepts '.alias' files.");
            }
            else
            {
                //the file is OK
                XmlDocument xdoc = new XmlDocument();

                xdoc.Load(arguments[0]);

                var args2 = new List<string>();

                args2.Add(xdoc.SelectSingleNode("/command/name").InnerText);
                Console.WriteLine("Name: " + args2.Last());

                args2.Add(xdoc.SelectSingleNode("/command/description").InnerText);
                Console.WriteLine("\nDescription: " + args2.Last());

                args2.Add(xdoc.SelectSingleNode("/command/executed").InnerText);
                Console.WriteLine("\nExecuted command: " + args2.Last());

                args2.Add(xdoc.SelectSingleNode("/command/arguments").InnerText);
                Console.WriteLine("\nArguments: " + args2.Last());

                Console.WriteLine("Proceed? (Y/N): ");

                if (Console.ReadKey().Key == ConsoleKey.Y)
                {
                    arguments = args2.ToArray();

                    Make();
                }
                else
                {
                    Console.WriteLine("Operation cancelled");
                }
            }
        }

        private static void Review()
        {
            if (arguments.Length < 1)
            {
                Console.WriteLine("Please supply the Alias name");
                Environment.Exit(0);
            }

            XmlDocument doc = new XmlDocument();

            doc.Load(installedRosterPath);

            var el = doc.SelectSingleNode($"commands/command[name={arguments[0]}]");

            if (el != null)
                Console.WriteLine("This command doesn't exist!");
            else
                Process.Start("notepad.exe", Path.Combine(path, arguments[0] + ".bat"));
        }

        private static void Full()
        {
            if (arguments.Length < 3)
            {
                Console.WriteLine("Please supply new Alias name, description, and a semicolon seperated list of arguments (or NaN for none)");
                Environment.Exit(0);
            }

            arguments = new string[] { arguments[0], arguments[1], "Place your code here!", arguments[2] };
            Make();
            Process.Start("notepad.exe", Path.Combine(path, arguments[0] + ".bat"));
        }

        private static void InstallCommand()
        {
            if (arguments.Length < 1)
            {
                Console.WriteLine("Use 'Alias common' for help with this command");
                Environment.Exit(0);
            }

            var onlineCommands = GetAllCommonCommands();

            if (arguments[0].ToLower() == "all")
            {
                foreach (var commonCommand in onlineCommands)
                {
                    arguments = new string[] { commonCommand.name, commonCommand.description, commonCommand.executed, commonCommand.arguments.Count() == 0 ? "NaN" : String.Join(";", commonCommand.arguments) };
                    Make();
                }
            }
            else
                if (onlineCommands.Where(x => x.name == arguments[0]).Count() == 1) //? if this Id exists
            {
                var wantedCommand = onlineCommands.Where(x => x.name == arguments[0]).Single();
                arguments = new string[] { wantedCommand.name, wantedCommand.description, wantedCommand.executed, wantedCommand.arguments.Count() == 0 ? "NaN" : String.Join(";", wantedCommand.arguments) };
                Make();
            }
            else
                Console.WriteLine("This Id doesn't exist!");
        }

        private static void ViewCommon()
        {
            //  In the not so famous words of Rhys O'Connor,
            //  'This is fucking awful guvnor!'

            Console.WriteLine("To install a common command, type 'alias install <command name>', or just 'alias install all'" + Environment.NewLine);

            var commands = GetAllCommonCommands();

            var formattedCommands = new List<formattedCommand>();

            commands.ToList().ForEach(x =>
            {
                formattedCommands.Add(new formattedCommand() { name = x.name, description = x.description, arguments = String.Join(";", x.arguments) });
            });

            Console.WriteLine(formattedCommands.ToMarkdownTable());
        }

        private static void InstallApp()
        {
            Console.WriteLine("Do you wish to install Alias? (Y/N): ");

            var assent = Console.ReadKey().Key;

            if (assent == ConsoleKey.Y)
            {
                #region appdirectory
                Console.WriteLine("Creating Application directory...");

                Directory.CreateDirectory(path);

                progress();
                #endregion

                #region environmentVariable
                var Envpath = Environment.GetEnvironmentVariable("Path", EnvironmentVariableTarget.User);

                Console.WriteLine("Updating Environment variable to include application folder");

                if (!Envpath.Contains(path))
                    Envpath = Envpath + ";" + path;

                Environment.SetEnvironmentVariable("Path", Envpath, EnvironmentVariableTarget.User);
                progress();
                #endregion

                #region installingExe
                Console.WriteLine("Installing executable");

                File.Copy(Path.Combine(Environment.CurrentDirectory, "alias.exe"), Path.Combine(path, "alias.exe"));
                progress();
                #endregion

                #region installRoster
                Console.WriteLine("Registering command roster...");
                progress();

                Console.WriteLine("Populating Roster...");

                XmlDocument doc = new XmlDocument();
                XmlNode root = doc.CreateElement("commands");

                doc.AppendChild(root);
                doc.Save(installedRosterPath);
                progress();
                #endregion

                #region config
                Console.WriteLine("Generating config file");

                /*
                 * <config>
                 *      <setting>
                 *          <name>SettingName</name>
                 *          <value>Foo</Foo>
                 *      </setting>
                 * </config>
                 */

                List<KeyValuePair<string, string>> settings = new List<KeyValuePair<string, string>>(); //<name, default>

                string settingsXML = "<config>";

                settings.Add(new KeyValuePair<string, string>("AllowExport", "true"));
                settings.Add(new KeyValuePair<string, string>("AllowLoad", "true"));
                settings.Add(new KeyValuePair<string, string>("UsageLogs", "true"));

                settings.ForEach(x =>
                {
                    settingsXML += $"<setting><name>{x.Key}</name><value>{x.Value}</value></setting>";
                });

                settingsXML += "</config>";

                File.WriteAllText(installedConfigPath, settingsXML);
                progress();
                Console.WriteLine("Generated config file");
                #endregion

                #region dependencies
                Console.WriteLine("Installing dependencies...");

                string[] dependencies = Directory.GetFiles(Environment.CurrentDirectory, "*.dll");

                foreach (var dep in dependencies)
                {
                    Console.WriteLine($"Installing {Path.GetFileName(dep)}...");
                    File.Copy(dep, Path.Combine(path, Path.GetFileName(dep)));
                    progress();
                }

                Console.WriteLine($"Installed {dependencies.Count()} dependencies");
                #endregion

                Console.WriteLine("Installation complete! The prompt needs to restart..." + Environment.NewLine);

                for (int i = 5; i > 0; i--)
                {
                    Console.WriteLine($"Console restarting in {i} " + (i == 1 ? "second" : "seconds"));
                    Thread.Sleep(1000);
                }
            }
        }

        private static void View()
        {
            var aliasList = new List<formattedCommand>();

            XmlDocument doc = new XmlDocument();

            doc.Load(Path.Combine(path, "commandRoster.xml"));

            XmlElement root = doc.DocumentElement;
            XmlNodeList nodes = root.SelectNodes("command");

            foreach (XmlNode node in nodes)
            {
                aliasList.Add(new formattedCommand()
                {
                    name = node.SelectSingleNode("name").InnerText,
                    description = node.SelectSingleNode("description").InnerText,
                    arguments = node.SelectSingleNode("arguments").InnerText
                });
            }

            if (aliasList.Count() == 0)
                Console.WriteLine("No Aliases here! type 'Alias make' to get started");
            else
                Console.WriteLine(aliasList.ToMarkdownTable());
        }

        private static void Remove()
        {
            if (arguments.Length < 1)
            {
                Console.WriteLine("Please supply alias name to remove");
                Environment.Exit(0);
            }

            #region loadRoster
            XmlDocument doc = new XmlDocument();
            doc.Load(installedRosterPath);
            XmlNode el = doc.SelectSingleNode($"/commands/command[name='{arguments[0]}']");
            #endregion

            if (el != null) //inverted from normal command checks
            {
                #region removeRegistration
                Console.WriteLine("Removing alias registration from Roster...");
                el.ParentNode.RemoveChild(el);
                doc.Save(installedRosterPath);
                progress();
                #endregion

                #region deleteFile
                Console.WriteLine("Removing Alias file...");
                File.Delete(Path.Combine(path, arguments[0] + ".bat"));
                progress();
                #endregion
            }
            else
            {
                Console.WriteLine("This command doesn't exist!");
            }
        }

        private static void Make()
        {
            if (arguments.Length < 4)
            {
                Console.WriteLine("Please specify <name>, <description>, <executed>, <argument1;argument2;argument3...> in that order");
                Environment.Exit(0);
            }

            #region loadRoster
            Console.WriteLine("Loading Roster...");
            XmlDocument doc = new XmlDocument();
            doc.Load(installedRosterPath);
            progress();
            #endregion

            XmlNode el = doc.SelectSingleNode($"/commands/command[name='{arguments[0]}']");
            Console.WriteLine("Checking for command existence...");
            progress();

            if (el != null) //? if this alias already exists
            {
                Console.WriteLine("An Alias with this name already exists.");
            }
            else
            {
                #region checkingExistence
                Console.WriteLine("This Alias doesn't exist. Checking system commands...");
                progress();

                foreach (var commandPath in Environment.GetEnvironmentVariable("PATH", EnvironmentVariableTarget.User).Split(';'))
                {
                    string[] extensions = new string[] { ".exe", ".com", ".exe", "", ".msc", ".cpl" };
                    var exists = false;

                    extensions.ToList().ForEach(extension =>
                    {
                        if (File.Exists(Path.Combine(commandPath, arguments[0] + extension)))
                        {
                            exists = true;
                        }
                    });

                    if (exists)
                    {
                        Console.WriteLine("This command already exists as a system command. Please choose a different name.");
                        Environment.Exit(0);
                    }
                }

                Console.WriteLine("This command doesn't exist as a system command. Creating Alias...");
                progress();
                #endregion

                #region registerXMLCommand
                Console.WriteLine("Registering roster...");

                XmlNode root = doc.SelectSingleNode("commands");

                XmlNode command = doc.CreateNode(XmlNodeType.Element, "command", "");

                XmlNode name = doc.CreateNode(XmlNodeType.Element, "name", "");
                XmlText nameValue = doc.CreateTextNode(arguments[0]);

                XmlNode description = doc.CreateNode(XmlNodeType.Element, "description", "");
                XmlText descriptionValue = doc.CreateTextNode(arguments[1]);

                XmlNode executed = doc.CreateNode(XmlNodeType.Element, "executed", "");
                XmlText executedValue = doc.CreateTextNode(arguments[2].Replace("\anl", "\r\n"));

                XmlNode xmlarguments = doc.CreateNode(XmlNodeType.Element, "arguments", "");
                XmlText argumentsValue = doc.CreateTextNode(arguments[3]);

                name.AppendChild(nameValue);
                description.AppendChild(descriptionValue);
                executed.AppendChild(executedValue);
                xmlarguments.AppendChild(argumentsValue);

                root.AppendChild(command);
                command.AppendChild(name);
                command.AppendChild(description);
                command.AppendChild(executed);
                command.AppendChild(xmlarguments);

                doc.Save(installedRosterPath);
                #endregion

                progress();

                Console.WriteLine("Creating command...");

                var commandName = arguments[0];
                var commandDescription = arguments[1];
                var commandExecuted = arguments[2];
                var commandArguments = arguments[3];

                File.WriteAllText(Path.Combine(path, commandName + ".bat"), String.Concat(commandExecuted.StartsWith("@echo off") ? "" : "@echo off\r\n", commandExecuted.Replace("\anl", Environment.NewLine)));



                Console.WriteLine($"Alias created! Run '{arguments[0]}' to use.");
            }
        }

        private static void uninstall()
        {
            Console.WriteLine("Are you sure you want to completely uninstall Alias? (Y/N):");
            var assent = Console.ReadKey();

            if (assent.Key == ConsoleKey.Y)
            {
                Process.Start("cmd.exe",
                    $"/C choice /C Y /N /D Y /T 3 & Del /F /Q {path} & rmdir {path}");

                Environment.Exit(0);
            }
        }
    }
}